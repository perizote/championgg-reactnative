import { StyleSheet } from 'react-native';

const CHAMPION_DETAIL_STYLES = StyleSheet.create({
    toolbar: {
        backgroundColor: '#3f51b5',
        height: 56,
    },
    container: {
        flex: 1,
    },
    loadingContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});

export default CHAMPION_DETAIL_STYLES;
