import React, {
    Component,
    View,
    Text,
    ToolbarAndroid,
    ScrollView,
    ProgressBarAndroid
} from 'react-native';
import {
  Cell,
  CustomCell,
  Section,
  TableView
} from 'react-native-tableview-simple';
import CHAMPION_DETAIL_STYLES from './ChampionDetail.styles';
import { DB_URLS, RIOT_URLS, FILE_EXTENSIONS, ENDPOINTS } from '../../common/urls';

export default class ChampionDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            championSpecs: [],
            loaded: false
        };
    }

    _getChampionSpecs() {
        return fetch(ENDPOINTS.championDetails)
            .then(response => response.json())
            .then(championDetails => {
                let championSpecs = championDetails.filter(champion => {
                    return this.props.id === champion.key
                });

                this.setState({
                    championSpecs: championSpecs,
                    loaded: true
                });
            })
            .done();
    }

    componentDidMount() {
        this._getChampionSpecs();
    }

    _renderLoadingView() {
        return (
            <View style={CHAMPION_DETAIL_STYLES.loadingContainer}>
                <ProgressBarAndroid
                    styleAttr="Inverse"
                    color={'#3f51b5'}
                />
            </View>
        );
    }

    render() {
        if (!this.state.loaded) {
            return this._renderLoadingView();
        }

        let championDetails = this.state.championSpecs.map((championDetail, index) => {
            return (
                <Section key={index} header={championDetail.role} sectionTintColor="#5C6BC0" headerTintColor="#fff">
                    <Cell title="Kills" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.kills}/>
                    <Cell title="Deaths" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.deaths}/>
                    <Cell title="Assists" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.assists}/>
                    <Cell title="Play Percent" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.playPercent}/>
                    <Cell title="Win Percent" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.winPercent}/>
                    <Cell title="Ban Rate" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.banRate}/>
                    <Cell title="Gold Earned" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.goldEarned}/>
                    <Cell title="Minions Killed" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.minionsKilled}/>
                    <Cell title="Enemy Jungle CS" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.neutralMinionsKilledEnemyJungle}/>
                    <Cell title="Team Jungle CS" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.neutralMinionsKilledTeamJungle}/>
                    <Cell title="Role Position" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.overallPosition}/>
                    <Cell title="Position Change" titleTintColor="#3f51b5" cellstyle="Subtitle" detail={championDetail.overallPositionChange}/>
                </Section>
            );
        });

        return (
            <View style={CHAMPION_DETAIL_STYLES.container}>
                <ToolbarAndroid
                    style={CHAMPION_DETAIL_STYLES.toolbar}
                    title={this.props.name}
                    navIcon={require('image!ic_arrow_back_white_24dp')}
                    onIconClicked={this.props.navigator.pop}
                    titleColor={'#FFFFFF'}/>
                <ScrollView>
                    <TableView>
                        {championDetails}
                    </TableView>
                </ScrollView>
            </View>
        );
    }
}
