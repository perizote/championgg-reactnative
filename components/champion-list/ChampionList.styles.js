import { StyleSheet } from 'react-native';

const CHAMPION_LIST_STYLES = StyleSheet.create({
    page: {
        flex: 1
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    rightContainer: {
        flex: 1,
    },
    title: {
        fontSize: 20,
        marginBottom: 8,
        textAlign: 'center',
    },
    year: {
        textAlign: 'center',
    },
    thumbnail: {
        width: 53,
        height: 81,
    },
    listView: {
        paddingTop: 0,
        backgroundColor: '#F5FCFF',
        flex: 1
    },
    toolbar: {
        backgroundColor: '#3f51b5',
        height: 56,
    },
});

export default CHAMPION_LIST_STYLES;
