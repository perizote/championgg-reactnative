import React, {
    Component,
    View,
    ListView,
    Text,
    Image,
    ProgressBarAndroid,
    TouchableHighlight,
    ToolbarAndroid
} from 'react-native';
import CHAMPION_LIST_STYLES from './ChampionList.styles';
import { DB_URLS, RIOT_URLS, FILE_EXTENSIONS, ENDPOINTS } from '../../common/urls';

export default class ChampionList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            championList: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            loaded: false
        };
    }

    _getChampionList() {
        return fetch(ENDPOINTS.championList)
            .then(response => response.json())
            .then(championList => {
                championList = championList.map(champion => {
                    champion.image = this.imageHostUrl + champion.key + FILE_EXTENSIONS.png;
                    return champion;
                });

                this.setState({
                    championList: this.state.championList.cloneWithRows(championList),
                    loaded: true
                });
            })
            .done();
    }

    componentDidMount() {
        this._getChampionList();
    }

    componentWillReceiveProps(nextProps) {
        this.imageHostUrl = RIOT_URLS.host + nextProps.version + RIOT_URLS.images.champion;
    }

    render() {
        if (!this.state.loaded) {
            return this._renderLoadingView();
        }

        return (
            <View style={CHAMPION_LIST_STYLES.page}>
                <ToolbarAndroid
                    style={CHAMPION_LIST_STYLES.toolbar}
                    title={this.props.title}
                    titleColor={'#FFFFFF'}
                />
                <ListView
                    dataSource={this.state.championList}
                    renderRow={this._renderChampion.bind(this)}
                    style={CHAMPION_LIST_STYLES.listView}
                />
            </View>
        );
    }

    _renderLoadingView() {
        return (
            <View style={CHAMPION_LIST_STYLES.container}>
                <ProgressBarAndroid
                    styleAttr="Inverse"
                    color={'#3f51b5'}
                />
            </View>
        );
    }

    _navigateToChampionDetail(champion) {
        this.props.navigator.push({
            params: {
                id: champion.key,
                name: champion.name
            },
            name: 'ChampionDetail'
        });
    }

    _renderChampion(champion) {
        return (
            <TouchableHighlight onPress={this._navigateToChampionDetail.bind(this, champion)}>
                <View style={CHAMPION_LIST_STYLES.container}>
                    <Image
                        source={{uri: champion.image}}
                        style={CHAMPION_LIST_STYLES.thumbnail}
                    />
                    <View style={CHAMPION_LIST_STYLES.rightContainer}>
                        <Text style={CHAMPION_LIST_STYLES.title}>{champion.name}</Text>
                        <Text style={CHAMPION_LIST_STYLES.year}>{champion.title}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        );
    }
}
