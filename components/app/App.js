import React, {
    Component,
    View,
    Navigator
} from 'react-native';
import APP_STYLES from './App.styles';
import { DB_URLS, ENDPOINTS } from '../../common/urls';
import ChampionList from '../champion-list/ChampionList';
import ChampionDetail from '../champion-detail/ChampionDetail';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            version: ''
        };
    }

    _getPatchVersion() {
        return fetch(ENDPOINTS.patchVersion)
            .then(response => response.json())
            .then(version => {
                this.setState({
                    version: version
                });
            })
            .done();
    }

    componentDidMount() {
        this._getPatchVersion();
    }

    _renderNavigatorScene(route, navigator) {
        switch (route.name) {
            case 'ChampionList':
                return (
                    <ChampionList
                        version={this.state.version}
                        navigator={navigator}
                        title="Champion List">
                    </ChampionList>
                );
            case 'ChampionDetail':
                return (
                    <ChampionDetail
                        id={route.params.id}
                        navigator={navigator}
                        title={route.params.name}>
                    </ChampionDetail>
                );
        }
    }

    render() {
        return (
            <Navigator
                initialRoute={{name: 'ChampionList'}}
                renderScene={this._renderNavigatorScene.bind(this)}
                configureScene={(route, routeStack) => Navigator.SceneConfigs.HorizontalSwipeJump}
            />
        );
    }
}
