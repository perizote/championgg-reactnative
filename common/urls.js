const DB_HOST = 'https://amber-fire-8604.firebaseio.com';

export const ENDPOINTS = {
    patchVersion: DB_HOST + '/patch.json',
    championList: DB_HOST + '/champions.json',
    championDetails: DB_HOST + '/championDetails.json'
};

export const DB_URLS = {
    patchVersion: DB_HOST + '/db/dd_patch.json',
    championList: DB_HOST + '/db/champions.json',
    items: DB_HOST + '/db/items.json',
    skills: DB_HOST + '/db/skills.json',
    masteries: DB_HOST + '/db/masteries.json',
    summoners: DB_HOST + '/db/summoners.json',
    championPages: DB_HOST + '/db/webchampionpages.json',
    statisticspages: DB_HOST + '/db/webstatisticspages.json'
};

export const RIOT_URLS = {
    host: 'https://ddragon.leagueoflegends.com/cdn/',
    images: {
        champion: '/img/champion/'
    }
};

export const FILE_EXTENSIONS = {
    png: '.png'
}
